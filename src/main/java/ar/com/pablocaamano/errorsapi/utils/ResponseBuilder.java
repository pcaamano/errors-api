package ar.com.pablocaamano.errorsapi.utils;

import ar.com.pablocaamano.errorsapi.rest.Error;
import ar.com.pablocaamano.errorsapi.rest.Meta;
import ar.com.pablocaamano.errorsapi.rest.Response;
import org.springframework.http.HttpMethod;

import java.util.LinkedList;
import java.util.List;

public class ResponseBuilder {

    Response response;
    Meta meta;
    List<Error> data;
    Error error;

    public ResponseBuilder(){
        response = new Response();
        meta = new Meta();
        data = new LinkedList<>();
        error = new Error();
    }

    public static ResponseBuilder init(){
        ResponseBuilder responseBuilder = new ResponseBuilder();
        return responseBuilder;
    }

    public ResponseBuilder withMetaMethod(HttpMethod method){
        meta.setMethod(method);
        return this;
    }

    public ResponseBuilder withMetaOperation(String operation){
        meta.setOperation(operation);
        return this;
    }

    public ResponseBuilder addData(Error error){
        data.add(error);
        return this;
    }

    public ResponseBuilder addDataList(List<Error> errorList){
        data = errorList;
        return this;
    }

    public  ResponseBuilder addErrorCode(String code){
        error.setCode(code);
        return this;
    }

    public  ResponseBuilder addErrorMessage(String message){
        error.setMessage(message);
        return this;
    }

    public  ResponseBuilder addErrorDetail(String detail){
        error.setDetail(detail);
        return this;
    }

    public  ResponseBuilder addServiceError(String serviceName){
        error.setServiceName(serviceName);
        return this;
    }

    public Response build(){
        response.setMetaData(meta);
        response.setData(data);
        response.setError(error);
        return this.response;
    }
}
