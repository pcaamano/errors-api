package ar.com.pablocaamano.errorsapi.controller;

import ar.com.pablocaamano.errorsapi.rest.Error;
import ar.com.pablocaamano.errorsapi.rest.Response;
import ar.com.pablocaamano.errorsapi.service.ErrorService;
import ar.com.pablocaamano.errorsapi.utils.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/api/errors")
public class MainController {

    @Autowired
    private ErrorService errorService;

    private Logger logger = LoggerFactory.getLogger(MainController.class);

    @GetMapping(value = "/all")
    public ResponseEntity<Response> getAllErrors(){
        String operation = "/api/errors/all";
        logger.info("[ERRORS-API] - Get all errors in DB.");
        try{
            List<Error>errorList = errorService.getAllError();
            if (errorList == null || errorList.isEmpty()){
                logger.info("[ERRORS-API] - Results not found.");
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.GET)
                        .withMetaOperation(operation)
                        .addErrorCode("ERR-404")
                        .addErrorMessage("Results not found.")
                        .addErrorDetail("No results were found in the database for the query made.")
                        .addServiceError("ERRORS-API")
                        .build()
                    ,HttpStatus.NOT_FOUND);
            }else {
                logger.info("[ERRORS-API] - Results listed.");
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.GET)
                        .withMetaOperation(operation)
                        .addDataList(errorList)
                        .build()
                    ,HttpStatus.OK);
            }
        }catch (Exception exception) {
            logger.error("[ERRORS-API] - Internal error.");
            return new ResponseEntity<Response>(
                ResponseBuilder.init()
                    .withMetaMethod(HttpMethod.GET)
                    .withMetaOperation(operation)
                    .addErrorCode("ERR-500")
                    .addErrorMessage("Internal error.")
                    .addErrorDetail(exception.getMessage())
                    .addServiceError("ERRORS-API")
                    .build()
                ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/get/{code}")
    public ResponseEntity<Response> getErrorByCode(@PathVariable String code){
        String operation = "/api/errors/get/".concat(code);
        logger.info("[ERRORS-API] - Get error by Id: ".concat(code));
        try {
            Error error = errorService.getErrorByCode(code);
            if (error == null){
                logger.info("[ERRORS-API] - Result not found.");
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.GET)
                        .withMetaOperation(operation)
                        .addErrorCode("ERR-404")
                        .addErrorMessage("Result not found.")
                        .addErrorDetail("Error with code ".concat(code).concat(" not found in the database."))
                        .addServiceError("ERRORS-API")
                        .build()
                    ,HttpStatus.NOT_FOUND);
            }else{
                logger.info("[ERRORS-API] - Error with code ".concat(code).concat(" found."));
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.GET)
                        .withMetaOperation(operation)
                        .addData(error)
                        .build()
                    ,HttpStatus.OK);
            }
        }catch (Exception exception){
            logger.error("[ERRORS-API] - Internal error.");
            return new ResponseEntity<Response>(
                ResponseBuilder.init()
                    .withMetaMethod(HttpMethod.GET)
                    .withMetaOperation(operation)
                    .addErrorCode("ERR-500")
                    .addErrorMessage("Internal error.")
                    .addErrorDetail(exception.getMessage())
                    .addServiceError("ERRORS-API")
                    .build()
                ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<Response> insertError(@RequestBody Error error){
        String operation = "/api/errors/insert";
        logger.info("[ERRORS-API] - Insert new error.");
        try{
            Error errorInserted = errorService.insertError(error);
            if(errorInserted == null){
                return new ResponseEntity<Response>(
                    ResponseBuilder.init().withMetaMethod(HttpMethod.POST)
                        .withMetaOperation(operation)
                        .addErrorCode("ERR-400")
                        .addErrorMessage("Request failed")
                        .addErrorMessage("Error object insertion validation failed.")
                        .addServiceError("ERRORS-API")
                        .build()
                    ,HttpStatus.BAD_REQUEST);
            }else {
                logger.info("[ERRORS-API] - Error ".concat(error.getCode()).concat(" inserted."));
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.POST)
                        .withMetaOperation(operation)
                        .addData(error)
                        .build()
                    , HttpStatus.OK);
            }
        }catch (Exception exception) {
            logger.error("[ERRORS-API] - Internal error.");
            return new ResponseEntity<Response>(
                ResponseBuilder.init()
                    .withMetaMethod(HttpMethod.POST)
                    .withMetaOperation(operation)
                    .addErrorCode("ERR-500")
                    .addErrorMessage("Internal error.")
                    .addErrorDetail(exception.getMessage())
                    .addServiceError("ERRORS-API")
                    .build()
                ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/update/{code}")
    public ResponseEntity<Response> updateError(
            @PathVariable String code,
            @RequestParam(value="message", required=false) String message,
            @RequestParam(value="detail", required=false) String detail,
            @RequestParam(value="serviceName", required=false) String serviceName){
        String operation = "/api/errors/update/".concat(code);
        logger.info("[ERRORS-API] - Update error with code ".concat(code));
        Error errorUpdate = new Error();
        errorUpdate.setMessage(message);
        errorUpdate.setDetail(detail);
        errorUpdate.setServiceName(serviceName);
        try {
            Error errorUpdated = errorService.updateError(code,errorUpdate);
            if (errorUpdated == null){
                logger.info("[ERRORS-API] - Update failed");
                return new ResponseEntity<Response>(
                    ResponseBuilder.init().withMetaMethod(HttpMethod.PUT)
                        .withMetaOperation(operation)
                        .addErrorCode("ERR-400")
                        .addErrorMessage("Update failed")
                        .addErrorDetail("Missing new data to perform the update.")
                        .addServiceError("ERRORS-API")
                        .build()
                    ,HttpStatus.BAD_REQUEST);
            }else {
                logger.info("[ERRORS-API] - Update success.");
                return new ResponseEntity<Response>(
                    ResponseBuilder.init().withMetaMethod(HttpMethod.PUT)
                        .withMetaOperation(operation)
                        .addData(errorUpdated)
                        .build()
                    ,HttpStatus.OK);
            }
        }catch (Exception exception){
            logger.error("[ERRORS-API] - Internal error.");
            return new ResponseEntity<Response>(
                ResponseBuilder.init()
                    .withMetaMethod(HttpMethod.PUT)
                    .withMetaOperation(operation)
                    .addErrorCode("ERR-500")
                    .addErrorMessage("Internal error.")
                    .addErrorDetail(exception.getMessage())
                    .addServiceError("ERRORS-API")
                    .build()
                ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/delete/{code}")
    public ResponseEntity<Response> deleteError(@PathVariable String code){
        String operation = "/api/errors/delete/".concat(code);
        logger.info("[ERRORS-API] - Delete error with code ".concat(code));
        try{
            Error errorDeleted = errorService.deleteError(code);
            if (errorDeleted == null){
                logger.info("[ERRORS-API] - Does not exist results with code ".concat(code));
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.DELETE)
                        .withMetaOperation(operation)
                        .addErrorCode("ERR-400")
                        .addErrorMessage("Can't process delete operation")
                        .addErrorDetail("Does not exist results with code ".concat(code))
                        .addServiceError("ERRORS-API")
                        .build()
                    ,HttpStatus.BAD_REQUEST);
            }else{
                logger.info("[ERRORS-API] - Error with code ".concat(code).concat(" was deleted."));
                return new ResponseEntity<Response>(
                    ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.DELETE)
                        .withMetaOperation(operation)
                        .addData(errorDeleted)
                        .build()
                    ,HttpStatus.OK);
            }
        }catch(Exception exception){
            logger.error("[ERRORS-API] - Internal error.");
            return new ResponseEntity<Response>(
                ResponseBuilder.init()
                    .withMetaMethod(HttpMethod.DELETE)
                    .withMetaOperation(operation)
                    .addErrorCode("ERR-500")
                    .addErrorMessage("Internal server Error")
                    .addErrorDetail(exception.getMessage())
                    .addServiceError("ERRORS-API")
                    .build()
                ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
