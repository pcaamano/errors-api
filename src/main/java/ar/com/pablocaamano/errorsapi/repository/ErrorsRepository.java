package ar.com.pablocaamano.errorsapi.repository;

import ar.com.pablocaamano.errorsapi.model.ErrorDTO;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import java.util.List;

@Document(collection = "errors")
public interface ErrorsRepository extends MongoRepository<ErrorDTO,String> {
    List<ErrorDTO> findAll();
    @Query("{code:?0}")
    ErrorDTO findOne(String id);
    ErrorDTO insert(ErrorDTO error);
    ErrorDTO save(ErrorDTO error);
    void delete(String id);
}
