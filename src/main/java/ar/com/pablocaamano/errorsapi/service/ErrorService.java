package ar.com.pablocaamano.errorsapi.service;

import ar.com.pablocaamano.errorsapi.rest.Error;
import java.util.List;


public interface ErrorService {
    List<Error> getAllError();
    Error getErrorByCode(String code);
    Error insertError(Error error);
    Error updateError(String code, Error error);
    Error deleteError(String code);
}
