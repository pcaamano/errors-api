package ar.com.pablocaamano.errorsapi.service;

import ar.com.pablocaamano.errorsapi.model.ErrorDTO;
import ar.com.pablocaamano.errorsapi.repository.ErrorsRepository;
import ar.com.pablocaamano.errorsapi.rest.Error;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ErrorServiceImpl implements ErrorService {

    @Autowired
    private ErrorsRepository errorsRepository;

    private ModelMapper modelMapper;

    public  ErrorServiceImpl(){
        modelMapper = new ModelMapper();
    }

    @Override
    public List<Error> getAllError() {
        List<ErrorDTO>dtoList = errorsRepository.findAll();
        List<Error>errorList = new LinkedList<>();
        for(ErrorDTO errorDTO: dtoList){
            Error error = modelMapper.map(errorDTO,Error.class);
            errorList.add(error);
        }
        return errorList;
    }

    @Override
    public Error getErrorByCode(String code) {
        ErrorDTO errorDTO = errorsRepository.findOne(code);
        if(errorDTO != null) {
            return modelMapper.map(errorDTO, Error.class);
        }else {
            return null;
        }
    }

    @Override
    public Error insertError(Error error) {
        ErrorDTO newError = modelMapper.map(error,ErrorDTO.class);
        ErrorDTO inserted = errorsRepository.insert(newError);
        if (inserted != null)
            return error;
        else return null;
    }

    @Override
    public Error updateError(String code, Error newParams) {
        ErrorDTO errorDTO = errorsRepository.findOne(code);
        if (newParams != null ){
            if(newParams.getMessage() != null)
                errorDTO.setMessage(newParams.getMessage());
            if(newParams.getDetail() != null)
                errorDTO.setDetail(newParams.getDetail());
            if(newParams.getServiceName() != null)
                errorDTO.setServiceName(newParams.getServiceName());
            errorsRepository.save(errorDTO);
            return modelMapper.map(errorDTO,Error.class);
        }else{
            return null;
        }
    }

    @Override
    public Error deleteError(String code) {
        ErrorDTO errorDTO = errorsRepository.findOne(code);
        if (errorDTO != null) {
            errorsRepository.delete(errorDTO.getId());
            return modelMapper.map(errorDTO, Error.class);
        }else {
            return null;
        }
    }
}
