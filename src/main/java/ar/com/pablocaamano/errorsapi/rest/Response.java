package ar.com.pablocaamano.errorsapi.rest;

import ar.com.pablocaamano.errorsapi.model.ErrorDTO;

import java.util.List;

public class Response {
    private Meta metaData;
    private List<Error>data;
    private Error error;

    public Meta getMetaData() {
        return metaData;
    }

    public void setMetaData(Meta metaData) {
        this.metaData = metaData;
    }

    public List<Error> getData() {
        return data;
    }

    public void setData(List<Error> data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
