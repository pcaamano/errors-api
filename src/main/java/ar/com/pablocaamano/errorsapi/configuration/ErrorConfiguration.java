package ar.com.pablocaamano.errorsapi.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ar.com.pablocaamano.errorsapi")
public class ErrorConfiguration {


}
