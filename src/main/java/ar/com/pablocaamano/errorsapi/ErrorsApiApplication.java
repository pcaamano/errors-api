package ar.com.pablocaamano.errorsapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErrorsApiApplication {

	private static final Logger logger = LoggerFactory.getLogger(ErrorsApiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ErrorsApiApplication.class, args);
		logger.info("[ERRORS-API] - application is now running...");
	}

}
